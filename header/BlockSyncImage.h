#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include "HLImage.h"

class BlockSyncImage :	public HLImage
{
protected:
	int W,H,N;				// width,height,width*height
	double *theta;			// 位相
	double *buf_theta;		// 位相(計算用)
	double *k1,*k2,*k3,*k4;	// RungeKutta計算用
	double *buf_k;			// RungeKutta計算用
	double Ks[4], Kw[4];	// 結合強度
	double dt = 0.1;		// 刻み幅

	void init();
	void rungekutta(bool);	// 近似計算(4次のRungeKutta法)
	void calc_kn(int n);	// RungeKuttaで使うKnを求める(k1,k2,k3,k4の順に呼び出すこと)
	inline int XY(int x, int y){ return y*W + x; };	// 画素の番号を返す

public:
	BlockSyncImage(char *filename) :		HLImage(filename){ init(); };
	BlockSyncImage(int w, int h) :			HLImage(w,h){ init(); };
	BlockSyncImage(const BlockSyncImage& blimg) :	HLImage(blimg){ init(); };		// コピーコンストラクタ
	~BlockSyncImage();

	void pixel_to_theta();	// 画素値→位相
	void theta_to_pixel();	// 位相→画素値
	void set_K(double Ks, double Kw);
	void set_K(double Ks[4], double Kw[4]);
	void sync(double);
	void resync(double);
	void input_theta(double *);
	double get_theta(int i) { return theta[i]; }
};

