#pragma once
#include "BlockSyncImage.h"

class SuperResolution
{
private:
	std::string imagename;			// 画像の名前

protected:
	BlockSyncImage *base_img;		// 元の画像(128x128)
	BlockSyncImage *low_img;		// 入力画像を、平均画素法(縮小)
	BlockSyncImage *near_img;		// low_img→最近傍法で拡大
	BlockSyncImage *output_img;		// 出力画像
	BlockSyncImage *correct_img;	// 正解画像
	BlockSyncImage *buf_img;		// 一時保存最適なパラメータかどうか
	
public:
	SuperResolution(char *);
	SuperResolution(char *, char *);
	SuperResolution(char *, char *, char *);
	SuperResolution(const SuperResolution &) = delete;
	~SuperResolution();
	void initialize(char *);
	void ext_imagename(char *);
	void ave();										// low_imgを作る
	void near();									// output_imgの発散処理前のものを作る
	void sr_only(double, double, std::string, bool, double);									// correct_imgを設定しない場合
	void sr(double, double, std::string, bool, double);
	void sr(double, double, std::string);			// パラメータ設定ver(特定の場合のを出したい時) Ks, Kw
	void sr(double, double, double);				// パラメータ設定ver(特定の場合のを出したい時) Ks, Kw, time
	string double_to_string(double d, int digits);	// 桁数を指定してdouble型をstringに変換
	string toupper(string);							// 引数の数字を大文字に
};

