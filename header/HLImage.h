#pragma warning(disable : 4996)
#ifndef _HLIMAGE_H_
#define _HLIMAGE_H_

#define BI_RGB			0L
#define BI_RLE8			1L
#define BI_RLE4			2L
#define BI_BITFIELDS	3L
#define MAXCOLORS		256

#include<iostream>
using namespace std;

	//------------------------------------------------------------------
	// 構造体
	//------------------------------------------------------------------
	typedef struct BITMAPFILEHEADER{ // BITMAPファイルヘッダ
        unsigned short	bfType;		// 識別子「BM」
        unsigned long	bfSize;		// ファイルサイズ
        unsigned short	bfReserved1;// 予約領域（未使用）
        unsigned short	bfReserved2;// 予約領域（未使用）
        unsigned long	bfOffBits;	// ビットマップデータの開始位置
	} BITMAPFILEHEADER;

	typedef struct BITMAPCOREHEADER{ // OS/2版情報ヘッダ
        unsigned long	bcSize;		// 情報ヘッダのサイズ（12）
        unsigned short	bcWidth;	// 画像の横幅
        unsigned short	bcHeight;	// 画像の縦幅
        unsigned short	bcPlanes;	// 画像のプレーン数
        unsigned short	bcBitCount;	// 1画素あたりのビット数
	} BITMAPCOREHEADER;

	typedef struct BITMAPINFOHEADER{ // Windows版情報ヘッダ
        unsigned long	biSize;			// 情報ヘッダのサイズ（40）
        unsigned long	biWidth;		// 画像の横幅
        unsigned long	biHeight;		// 画像の縦幅
        unsigned short	biPlanes;		// 画像のプレーン数
        unsigned short	biBitCount;		// 1画素あたりのビット数
        unsigned long	biCompression;	// 圧縮形式
        unsigned long	biSizeImage;	// 画像データ部のサイズ
        unsigned long	biXPelsPerMeter;// 横方向の解像度（1mあたりの画素数）
        unsigned long	biYPelsPerMeter;// 縦方向の解像度（1mあたりの画素数）
        unsigned long	biClrUsed;		// 格納されているパレットの色数
        unsigned long	biClrImportant;	// 重要なパレットのインデックス
	} BITMAPINFOHEADER;

	typedef struct HLPIXEL{	// 2D画素
		int	pix;
		int pix_R;
		int pix_G;
		int pix_B;
	} HLPIXEL;
	//------------------------------------------------------------------
	// HLImageクラス（1枚画像の処理）
	//------------------------------------------------------------------
	class HLImage{
	protected:
		int width, height;	// 画像の横幅、縦幅
		short bit;			// 1画素のバイト数

		HLPIXEL *pixels;	// 画素データ
	public:
		HLImage(){ init_pixel(); }		// コンストラクタ
		HLImage(int,int);				// コンストラクタ（新規作成版）
		HLImage(int,int,int*);			// コンストラクタ（外部入力版）
		HLImage(char *);				// コンストラクタ（ファイル入力版(BMP)）
		HLImage(const HLImage&);		// コピーコンストラクタ
		~HLImage(){ delete_pixel(); }	// デストラクタ
		void init_pixel(void);			// 初期化
		void create_pixel(int,int);		// メモリ確保(横幅、縦幅)
		void delete_pixel(void);		// 解放
		void zero_pixel(void);			// すべての値を0にする

		bool load_2byte(unsigned short *, FILE *);	// ファイル2バイト読込
		bool load_4byte(unsigned long *, FILE *);	// ファイル4バイト読込
		bool load_bmp(char *);						// bitmapファイルを読み込む
		bool save_2byte(unsigned short, FILE *);	// ファイル2バイト読込
		bool save_4byte(unsigned long, FILE *);		// ファイル4バイト読込
		bool save_bmp(char *);						// bitmapファイルを書き込む
		void output_console(void);					// コンソール画面へ表示
		bool export_histogram_csv(char *);			// ヒストグラム情報を出力

		// Setter, Getter
		int get_width(void){ return width; }		// widthを出力
		void set_width(int val){ width = val; }		// widthを入力
		int get_height(void){ return height; }		// heightを出力
		void set_height(int val){ height = val; }	// heightを入力
		short get_bit(void) { return bit; }
		void set_bit(short val) { bit = val; }
		int get_pixel(int,int);						// 指定した位置のデータを出力
		void set_pixel(int,int,int);				// 指定した位置にデータを入力
		void get_pixel_array(int,int,int*);			// データを配列に出力する（すべて）
		void set_pixel_array(int,int,int*);			// データを配列で入力する（すべて）
		double calc_mse(const HLImage &obj);		// MSEを計算
		double calc_nmse(const HLImage &obj);		// NMSE(MSE正規化版)を計算
		double calc_psnr(const HLImage &obj);		// PSNRを計算
		double calc_ssim(const HLImage &obj);		// SSIMを計算
		int reflect(int, int, int);
	};

#endif