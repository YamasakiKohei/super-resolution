#pragma once
#include "BlockSyncImage.h"

class ImageProcessing
{
private:
	std::string imagename;			//画像の名前

protected:
	BlockSyncImage *base_img;		//元の画像
	BlockSyncImage *output_img;

public:
	ImageProcessing(char *);
	~ImageProcessing();
	void bicubic();
	void ave();
	void lanczos();
	string get_filepath(char *);
};
