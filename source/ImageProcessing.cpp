#include "../header/ImageProcessing.h"

ImageProcessing::ImageProcessing(char *filepath)
{
	std::string fullpath = filepath;
	int path_length = fullpath.find_last_of("/") + 1;
	int ext_length = fullpath.find_last_of(".");
	imagename = fullpath.substr(path_length, ext_length - path_length);

	base_img = new BlockSyncImage(filepath);
}

ImageProcessing::~ImageProcessing()
{
	delete base_img;
	delete output_img;
}

// バイキュービック法による拡大
void ImageProcessing::bicubic()
{
	double a = -1.0;
	double param[5] = { a + 3.0, a + 2.0, -a * 4.0, a * 8.0, a * 5.0 };
	double scale = 2.0;
	
	output_img = new BlockSyncImage(base_img->get_width() * 2, base_img->get_height() * 2);
	
	for (int y = 0; y < output_img->get_height(); y++)
	{
		for (int x = 0; x < output_img->get_width(); x++)
		{
			// 元画像における画素の対応する場所
			double x0 = x / scale;
			double y0 = y / scale;

			// 基点とする画素
			int xBase = (int)x0;
			int yBase = (int)y0;

			int mono = 0;  // 補間する画素

				double pixel = 0.0;

				// 基点の周辺16画素を取得して処理
				for (int i = -1; i <= 2; i++)
				{
					for (int j = -1; j <= 2; j++) 
					{
						// 実際に処理する画素を設定
						int xCurrent = xBase + i;
						int yCurrent = yBase + j;

						// 距離決定
						double distX = fabs(xCurrent - x0);
						double distY = fabs(yCurrent - y0);

						// 重み付け
						double weight = 0.0;  // 重み変数

						// まずはX座標の距離で重みを設定
						// 1以下、2以下のとき、それぞれ重み付け
						if (distX <= 1.0) 
							weight = 1.0 - param[0] * distX * distX + param[1] * distX * distX * distX;
						else if (distX <= 2.0)
							weight = param[2] + param[3] * distX - param[4] * distX * distX + a * distX * distX * distX;
						else 
							continue;  // 何も処理しないので、次のループへ

						// Y座標の距離で重みを設定
						if (distY <= 1.0) 
							weight *= 1.0 - param[0] * distY * distY + param[1] * distY * distY * distY;
						else if (distY <= 2.0) 
							weight *= param[2] + param[3] * distY - param[4] * distY * distY + a * distY * distY * distY;
						else 
							continue;

						if (xCurrent < 0 || xCurrent > base_img->get_width() - 1)
							xCurrent = xBase;

						if (yCurrent < 0 || yCurrent > base_img->get_height() - 1)
							yCurrent = yBase;

						// 実際に画素を取得
						int pixel_process = base_img->get_pixel(xCurrent, yCurrent);

						// 重みをかけて足し合わせる
						pixel += pixel_process * weight;
					}
				}
				// 画素を結合する
				pixel = (pixel > 255) ? 255 : (pixel < 0) ? 0 : pixel;
				mono += (int)pixel;
				output_img->set_pixel(x, y, mono);
		}
	}
	output_img->save_bmp((char *)get_filepath("bicubic").c_str());
}

void ImageProcessing::lanczos()
{
	int n = 3; // N値(Lanczos-N)
	int nx = n - 1;
	double scale = 2.0;

	output_img = new BlockSyncImage(base_img->get_width() * 2, base_img->get_height() * 2);

	for (int y = 0; y < output_img->get_width(); y++)
	{
		for (int x = 0; x < output_img->get_height(); x++)
		{
			// 元画像における画素の対応する場所
			double x0 = x / scale;
			double y0 = y / scale;

			// 基点とする画素
			int xBase = (int)x0;
			int yBase = (int)y0;

			int mono = 0;  // 補間する画素

			double w_total = 0.0;
			double pixel = 0.0;

			// 周辺(a*2)^2画素を取得して処理
			for (int i = -nx; i <= n; i++)
			{
				for (int j = -nx; j <= n; j++)
				{
					// 実際に処理する画素を設定
					int xCurrent = xBase + i;
					int yCurrent = yBase + j;

					// 距離決定
					double distX = fabs(xCurrent - x0);
					double distY = fabs(yCurrent - y0);

					// 重み付け
					double weight = 0.0;  // 重み変数


					if (distX == 0.0)
						weight = 1.0;
					else if (distX < n)
					{
						double dPIx = M_PI * distX;
						weight = (sin(dPIx) * sin(dPIx / n) / (dPIx * (dPIx / n)));
					}
					else
						continue;  // 何も処理しないので、次のループへ

								   // Y座標の距離で重みを設定
					if (distY == 0.0)
						weight = 1.0;
					else if (distY < n)
					{
						double dPIy = M_PI * distY;
						weight = (sin(dPIy) * sin(dPIy / n) / (dPIy * (dPIy / n)));
					}
					else
						continue;

					if (xCurrent < 0 || xCurrent > base_img->get_width() - 1)
						xCurrent = xBase;

					if (yCurrent < 0 || yCurrent > base_img->get_height() - 1)
						yCurrent = yBase;

					// 実際に画素を取得
					int pixel_process = base_img->get_pixel(xCurrent, yCurrent);

					// 重みを足し合わせる
					w_total += weight;
				}
			}
			if (w_total != 0.0)	pixel /= w_total;
			pixel = (pixel > 255) ? 255 : (pixel < 0) ? 0 : pixel;
			mono += (int)pixel;
			output_img->set_pixel(x, y, mono);
		}
	}
	output_img->save_bmp((char *)get_filepath("lanczos").c_str());
}

void ImageProcessing::ave()
{
	int sum;

	output_img = new BlockSyncImage(base_img->get_width() / 2, base_img->get_height() / 2);

	for (int i = 0; i < base_img->get_height() / 2; i++)
	{
		for (int j = 0; j < base_img->get_width() / 2; j++)
		{
			sum = base_img->get_pixel(j * 2, i * 2) + base_img->get_pixel(j * 2 + 1, i * 2) + base_img->get_pixel(j * 2 + 1, i * 2) + base_img->get_pixel(j * 2 + 1, i * 2 + 1);
			output_img->set_pixel(j, i, (int)sum / 4.0 + 0.5);
		}
	}
	output_img->save_bmp((char *)get_filepath("ave").c_str());
}

string ImageProcessing::get_filepath(char* processname)
{
	std::string filepath = "outputs/" + imagename + "_" + processname + ".bmp";
	return filepath;
}