#include <fstream>
#include <iostream>
#include <math.h>

#include "../header/HLImage.h"

using namespace std;


	//------------------------------------------------------------------
	// HLImageクラス（1枚画像の処理）の実装
	//------------------------------------------------------------------
	HLImage::HLImage(int w, int h)
	{ // コンストラクタ（新規作成版）
		init_pixel();
		create_pixel(w,h);
	}
	HLImage::HLImage(int w, int h, int *pix)
	{ // コンストラクタ（外部入力版）
		init_pixel();
		create_pixel(w,h);
		for(int i=0; i<h; i++)
			for(int j=0; j<w; j++)
				pixels[i*width+j].pix = pix[i*width+j];
	}
	HLImage::HLImage(char *filename)
	{ // コンストラクタ（ファイル入力版(BMP)）
		if(load_bmp(filename)){
			printf("読込成功「%s」\n",filename);
		}else{
			printf("読込失敗「%s」\n",filename);
		}
	}
	HLImage::HLImage(const HLImage &obj)
	{ // コピーコンストラクタ
		init_pixel();
		create_pixel(obj.width,obj.height);
		for(int i=0; i<obj.height; i++)
			for(int j=0; j<obj.width; j++)
				pixels[i*width+j].pix = obj.pixels[i*obj.width+j].pix;
	}
	void HLImage::init_pixel(void)
	{ // 初期化
		width = height = -1;
		pixels = NULL;		
	}
	void HLImage::create_pixel(int w, int h)
	{ // メモリを確保する
		delete_pixel();
		width = w;	height = h;
		pixels = new HLPIXEL[w*h];
		zero_pixel();
	}

	void HLImage::delete_pixel(void)
	{ // 解放
		if(pixels != NULL){	// メモリが確保されている場合
			// メモリを削除する
			delete [] pixels;
			pixels = NULL;
		}
		init_pixel();
	}
	void HLImage::zero_pixel(void)
	{ // 全ての値を0にする
		if(pixels != NULL){
			for(int i=0; i<height; i++)
				for(int j=0; j<width; j++)
					pixels[i*width+j].pix = 0;
		}
	}
	bool HLImage::load_2byte(unsigned short *res, FILE *fp)
	{ // 2バイト読込
		int i,c;
		int val[2];

		for(i=0;i<2;i++) {
			c=fgetc(fp);
			if(c==EOF) return false;
			val[i]=c;
		}
		*res=val[1]*256+val[0];
		return true;
	}
	bool HLImage::load_4byte(unsigned long *res, FILE *fp)
	{ // 4バイト読込
		int i,c;
		int val[4];
		unsigned long tmp=0;

		for(i=0;i<4;i++) {
			c=fgetc(fp);
			if(c==EOF) return false;
			val[i]=c;
		}
		tmp=0;
		for(i=3;i>=0;i--) {
			tmp*=256;
			tmp+=val[i];
		}
		*res=tmp;
		return true;
	}
	bool HLImage::save_2byte(unsigned short val, FILE *fp)
	{ // 2バイト書込
		int i,c;

		c=val;
		for(i=0;i<2;i++) {
			fputc(c%256,fp);
			c/=256;
		}
		return true;
	}
	bool HLImage::save_4byte(unsigned long val, FILE *fp)
	{ // 4バイト書込
		int i,c;

		c=val;
		for(i=0;i<4;i++) {
			fputc(c%256,fp);
			c/=256;
		}
		return true;
	}
	bool HLImage::load_bmp(char *filename)
	{ // BMPファイルを読み込む
		FILE *fp;
		int isPM=false;	// BMPの形式を記録するフラグ

		int palet_r[MAXCOLORS];
		int palet_g[MAXCOLORS];
		int palet_b[MAXCOLORS];

		//BITMAPFILEHEADER BMPFile;
		BITMAPINFOHEADER BMPInfo;
		//BITMAPCOREHEADER BMPCore;

		unsigned short	HEAD_bfType;
		unsigned long	HEAD_bfSize;
		unsigned short	HEAD_bfReserved1;
		unsigned short	HEAD_bfReserved2;
		unsigned long	HEAD_bfOffBits;
		unsigned long	INFO_bfSize;
		
		// ファイルオープン
		if((fp=fopen(filename,"rb"))==NULL) {
			return false;	// 読込失敗
		}
		// BMPファイルのチェック
		if(!load_2byte(&HEAD_bfType,fp)){
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}
		if(HEAD_bfType != 0x4d42){
			fclose(fp);	return false;	// BMPでないため終了
		}
		// ヘッダ部のサイズ読込
		if(!load_4byte(&HEAD_bfSize,fp)){
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}		
		// 予約用領域（未使用）読込
		if(!load_2byte(&HEAD_bfReserved1,fp)){
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}
		// 予約用領域（未使用）読込
		if(!load_2byte(&HEAD_bfReserved2,fp)){
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}
		// オフセット読込
		if(!load_4byte(&HEAD_bfOffBits,fp)){
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}	
		// ヘッダ部のサイズ読込
		if(!load_4byte(&INFO_bfSize,fp)){
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}
		// ヘッダ部のサイズが規定外ならばエラーとする
		if(INFO_bfSize == 40/*sizeof(BITMAPINFOHEADER)*/ 
		|| INFO_bfSize == 12/*sizeof(BITMAPCOREHEADER)*/) {
			BMPInfo.biSize =	INFO_bfSize;	// BMPInfoの情報ヘッダサイズに値を格納
			if(INFO_bfSize == sizeof(BITMAPCOREHEADER)) {
				// BITMAPCOREHEADER形式の場合
				unsigned short tmp;
				isPM =	true;
				// 画像の横幅
				if(!load_2byte(&tmp,fp)){
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				BMPInfo.biWidth=tmp;
				// 画像の縦幅
				if(!load_2byte(&tmp,fp)){
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				BMPInfo.biHeight=tmp;
				// 画像のプレーン数
				if(!load_2byte(&(BMPInfo.biPlanes),fp)){
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// １画素あたりのビット数
				if(!load_2byte(&(BMPInfo.biBitCount),fp)){
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
			}else{	
				// BITMAPINFOHEADER形式の場合
				// 画像の横幅
				if(!load_4byte(&(BMPInfo.biWidth),fp)){
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// 画像の縦幅
				if(!load_4byte(&(BMPInfo.biHeight),fp)){
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// 画像のプレーン数
				if(!load_2byte(&(BMPInfo.biPlanes),fp)) {
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// １画素あたりのビット数
				if(!load_2byte(&(BMPInfo.biBitCount),fp)) {
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
			}
			// BITMAPINFOHEADERの場合のみ存在する情報を読み込む
			if(!isPM) {
				// 圧縮形式
				if(!load_4byte(&(BMPInfo.biCompression),fp)) {
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// 画像データ部のサイズ
				if(!load_4byte(&(BMPInfo.biSizeImage),fp)) {
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// X方向の解像度
				if(!load_4byte(&(BMPInfo.biXPelsPerMeter),fp)) {
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// Y方向の解像度
				if(!load_4byte(&(BMPInfo.biYPelsPerMeter),fp)) {
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// 格納されているパレットの色数
				if(!load_4byte(&(BMPInfo.biClrUsed),fp)) {
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
				// 重要なパレットのインデックス
				if(!load_4byte(&(BMPInfo.biClrImportant),fp)) {
					fclose(fp);	return false;	// 読み込み失敗のため終了
				}
			}
		}else{ //ヘッダ部のサイズが規定外
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}

		set_bit(BMPInfo.biBitCount);			// 1画素のビット数をセット

		int mx,my,dep;
		int	colors;
		mx=BMPInfo.biWidth;
		my=BMPInfo.biHeight;
		dep=BMPInfo.biBitCount;
		// 256色，フルカラー以外サポート外
		if(dep!=8 && dep!=24) {
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}
		// 非圧縮形式以外はサポート外
		if(BMPInfo.biCompression!=BI_RGB) {
			fclose(fp);	return false;	// 読み込み失敗のため終了
		}
		// ヘッダ部にパレットサイズの情報がない場合は１画素あたりのビット数から求める
		if(BMPInfo.biClrUsed==0) {
			switch (BMPInfo.biBitCount) {
			case 1:	colors	= 2;	break;
			case 4:	colors	= 16;	break;
			case 8:	colors	= 256;	break;
			default:colors	= 0;	break;
			}
		}
		else {
			colors	= BMPInfo.biClrUsed;
		}
		// パレット情報の読み込み
		// BMPの種類によってフォーマットが異なるので処理をわける
		if (!isPM)	{
			for(int i=0;i<colors;i++) {
				int c;
				// Blue成分
				if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
				palet_b[i]=c;
				// Green成分
				if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
				palet_g[i]=c;
				// Red成分
				if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
				palet_r[i]=c;
				// あまり
				if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
			}
		}else{
			for(int i=0;i<colors;i++) {
				int c;
				// Blue成分
				if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
				palet_b[i]=c;
				// Green成分
				if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
				palet_g[i]=c;
				// Red成分
				if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
				palet_r[i]=c;
			}
		}
		// 画像データの作成
		init_pixel();
		create_pixel(mx,my);
		
		int pad, mxb;

		switch(dep) {
		case 32: mxb = mx*4;				break;
		case 24: mxb = ((mx*3)+3)/4*4;		break;
		case 16: mxb = (mx+1)/2*2;			break;
		case 8:	 mxb = (mx+3)/4*4;			break;
		case 4:  mxb = (((mx+1)/2)+3)/4*4;	break;
		case 1:  mxb = (((mx+7)/8)+3)/4*4;	break;
		}
		pad=mxb-mx*dep/8;
		for(int y=my-1;y>=0;y--) {
			for(int x=0;x<mx;x++) {
				int c;
				int setcolor_r, setcolor_g, setcolor_b;
				if(dep==8) {	// 256色形式の場合はパレットからRGB値を求める
					if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
					setcolor_r=palet_r[c];
					setcolor_g=palet_g[c];
					setcolor_b=palet_b[c];
					pixels[y*width+x].pix = setcolor_r;
				}else if(dep==24) {
					if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
					setcolor_b=c;
					if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
					setcolor_g=c;
					if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
					setcolor_r=c;
					// NTSC変換
					pixels[y*width+x].pix = int(0.298912 * setcolor_r + 0.586611 * setcolor_g + 0.114478 * setcolor_b +0.5);
				}
			}
			// Padding部の読み飛ばし
			for(int i=0;i<pad;i++) {
				int c;
				if((c=fgetc(fp))==EOF){ fclose(fp); return false;} // 読み込み失敗のため終了
			}
		}
		fclose(fp);	// ファイルクローズ
		printf("%d,%d\n",width,height);
		return true;	// 読込成功
	}
	bool HLImage::save_bmp(char *filename)
	{ // BMPファイルを書き込む
		FILE *fp;
		BITMAPFILEHEADER bfn;
		int w,h,rw;
		int pad;
		int dep;	// １画素あたりのビット数
		int pbyte;	//１要素あたりのバイト数
		int palsize;	// パレットサイズ（未実装）
		int x,y,i;

		w=width;
		h=height;
		dep=24;

		// フルカラー以外サポート外
		if(dep!=24) { return false; } // 書き込み失敗のため終了
		// フルカラー以外のことを若干考慮しているが未実装．フルカラーのみなら、本来-- end -- の部分まで不要
		if(dep==24) {	pbyte=1; }
		else {			pbyte=8/8; }
		if(dep>=24) {	palsize=0; }
		else {			palsize=256; }
		// -- end --		
		// パディングを考慮した１列分に必要なバイト数
		switch(dep) {
		case 32: rw = w*4;					break;
		case 24: rw = ((w*3)+3)/4*4;		break;
		case 16: rw = (w+1)/2*2;			break;
		case 8:  rw = (w+3)/4*4;			break;
		case 4:  rw = (((w+1)/2)+3)/4*4;	break;
		case 1:  rw = (((w+7)/8)+3)/4*4;	break;
		}
		// ヘッダ部の設定（一部のみ）
		bfn.bfType=0x4d42;	//'BM'
		bfn.bfSize=14+40+/*sizeof(BITMAPFILEHEADER) +sizeof(BITMAPINFOHEADER) +*/
				   palsize*4/*sizeof(RGBQUAD)*/ +
				   rw*h*pbyte;
		bfn.bfReserved1=0;
		bfn.bfReserved2=0;
		bfn.bfOffBits=14+40/*sizeof(BITMAPFILEHEADER) +sizeof(BITMAPINFOHEADER)*/ +
					  palsize*4/*sizeof(RGBQUAD)*/;

		if((fp=fopen(filename,"wb"))==NULL){
			 fclose(fp); return false; // 書き込み失敗のため終了
		}
		// ヘッダ部の書き出し
		save_2byte(bfn.bfType,fp);
		save_4byte(bfn.bfSize,fp);
		save_2byte(bfn.bfReserved1,fp);
		save_2byte(bfn.bfReserved2,fp);
		save_4byte(bfn.bfOffBits,fp);
		save_4byte(40/*sizeof(BITMAPINFOHEADER)*/,fp);	//biSize
		save_4byte(w,fp);		// biWidth
		save_4byte(h,fp);		// biHeight
		save_2byte(1,fp);		// biPlanes
		save_2byte(dep,fp);		// biBitCount
		save_4byte(BI_RGB,fp);	// biCompression
		save_4byte(0,fp);		// biSizeImage
		save_4byte(300,fp);		// biXPelsPerMeter
		save_4byte(300,fp);		// biYPelsPerMeter
		save_4byte(0,fp);		// biClrUsed
		save_4byte(0,fp);		// biClrImportant

		// 必要なパディングのサイズ
		pad=rw-w*dep/8;
		// 画像データの書き出し
		for(y=h-1;y>=0;y--){
			for(x=0;x<w;x++){
				fputc(pixels[y*width+x].pix,fp);
				fputc(pixels[y*width+x].pix,fp);
				fputc(pixels[y*width+x].pix,fp);
			}
			// Padding部の出力
			for(i=0;i<pad;i++) {
				fputc(0,fp);
			}
		}
		fclose(fp);
		return true;	// 書込失敗
	}

	void HLImage::output_console(void)
	{ // コンソール画面へ表示する

		printf("pix[Height Pos][Width Pos]\n");
		for(int i=0; i<height; i++)
			for(int j=0; j<width; j++){
					printf("pix[%4d][%4d]=%3d\n ",i,j,pixels[i*width+j].pix);
			}
	}
	bool HLImage::export_histogram_csv(char *filename)
	{ // ヒストグラム情報を出力
		FILE *fp;
		int  *tmp;
		if(pixels==NULL){
			// データがない場合はエラーを返す
			return false;
		}
		if((fp=fopen(filename,"w"))==NULL){
			 fclose(fp); return false; // 書き込み失敗のため終了
		}

		tmp = new int[256];	// 統計用一時メモリの確保
		// 統計用一時メモリの初期化
		for(int i=0; i<256; i++)
			tmp[i]=0;
		// 数え上げ
		for(int i=0; i<height; i++)
			for(int j=0; j<width; j++)
				tmp[pixels[i*width+j].pix]++;
		// ファイル書込み
		fprintf( fp, "Pixel値,freq\n" );
		for(int i=0; i<256; i++)
			fprintf(fp, "\"%d\",%d\n", i, tmp[i]);

		delete tmp;	// 統計用一時メモリの解放
		fclose(fp);	// ファイルクローズ
		return true;
	}
	int HLImage::get_pixel(int wp, int hp)
	{ // 指定した位置のデータを出力する
		if(pixels != NULL){
			if(wp>=0&&wp<width&&hp>=0&&hp<height){
				return pixels[hp*width+wp].pix;
			}
		}
		return -1;
	}
	void HLImage::set_pixel(int wp, int hp, int val)
	{ // 指定した位置にデータを入力する
		if(pixels != NULL){
			if(wp>=0&&wp<width&&hp>=0&&hp<height){
				pixels[hp*width+wp].pix = val;
			}
		}
	}
	void HLImage::get_pixel_array(int w, int h, int *ary)
	{ // データを配列に出力する（すべて）
		if(pixels != NULL){
			if(width==w&&height==h){
				for(int i=0; i<height; i++)
					for(int j=0; j<width; j++)
						ary[i*width+j] = pixels[i*width+j].pix;
			}
		}
	}
	void HLImage::set_pixel_array(int w, int h, int *ary)
	{ // データを配列で入力する（すべて）
		if(pixels != NULL){
			if(width==w&&height==h){
				for(int i=0; i<height; i++)
					for(int j=0; j<width; j++)
						pixels[i*width+j].pix = ary[i*width+j];
			}
		}
	}
	double HLImage::calc_mse(const HLImage &obj)
	{ // MSEを計算・出力
		double mse = 0.0;	// MSE

		for (int i = 0; i<height; i++)
			for (int j = 0; j<width; j++)
			{
				mse += pow((double)(pixels[i*width + j].pix - obj.pixels[i*width + j].pix), 2);
			}
		mse /= (width*height);
		return mse;
	}
	double HLImage::calc_nmse(const HLImage &obj)
	{
		double diff = 0.0;
		double div = 0.0;
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				diff += pow(((double)pixels[i*width + j].pix - obj.pixels[i*width + j].pix), 2);
				div += pow(((double)obj.pixels[i*width + j].pix), 2);
			}
		}
		return diff / div;
	}
	double HLImage::calc_psnr(const HLImage &obj)
	{
		double mse, psnr;
		mse = calc_mse(obj);
		int max = 255;
		psnr = 20 * log10(max / sqrt(mse));
		//cout << "PSNR = " << psnr << endl;
		return psnr;
	}

	double HLImage::calc_ssim(const HLImage &obj)
	{
		double c1 = pow(0.01 * 255.0, 2);
		double c2 = pow(0.03 * 255.0, 2);

		int m = 2;	// 注目画素から距離mの画素を領域とする

		double sum = 0.0;

		int mx, my;

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				double ave1 = 0.0, ave2 = 0.0;	// 平均
				double var1 = 0.0, var2 = 0.0;	// 分散
				double cov = 0.0;				// 共分散

				for (int dy = -m; dy <= m; dy++)
				{
					for (int dx = -m; dx <= m; dx++)
					{
						mx = reflect(x + dx, 0, width);
						my = reflect(y + dy, 0, height);

						ave1 += pixels[mx + my * height].pix;
						ave2 += obj.pixels[mx + my * height].pix;
					}
				}

				ave1 /= pow(m * 2.0 + 1.0, 2.0);	// pow(m * 2.0 + 1.0, 2.0) → 領域の画素数
				ave2 /= pow(m * 2.0 + 1.0, 2.0);

				for (int dy = -m; dy <= m; dy++)
				{
					for (int dx = -m; dx <= m; dx++)
					{
						mx = reflect(x + dx, 0, width);
						my = reflect(y + dy, 0, height);

						var1 += pow(pixels[mx + my * height].pix - ave1, 2.0);
						var2 += pow(obj.pixels[mx + my *height].pix - ave2, 2.0);
						cov += (pixels[mx + my * height].pix - ave1) * (obj.pixels[mx + my * height].pix - ave2);
					}
				}
				var1 /= pow(m * 2.0 + 1.0, 2);
				var2 /= pow(m * 2.0 + 1.0, 2);
				cov /= pow(m * 2.0 + 1.0, 2);

				sum += ((2.0 * ave1 * ave2 + c1) * (2.0 * cov + c2)) / ((pow(ave1, 2.0) + pow(ave2, 2.0) + c1) * (var1 + var2 + c2));
			}
		}
		return sum /= (width * height);
	}

	// 画素番号を返す オーバーしている場合はオーバーした分だけ反射したところの番号を返す
	int HLImage::reflect(int x, int min, int max)
	{
		while (x < min || x >= max)
		{
			if (x < min)	x = min + (min - x - 1);
			if (x >= max)	x = max + (max - x - 1);
		}
		return x;
	}
	
