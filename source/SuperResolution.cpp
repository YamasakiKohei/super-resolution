#include "../header/SuperResolution.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <functional>
#include <algorithm>
#include <string>

// 1枚の画像から超解像するタイプ
SuperResolution::SuperResolution(char *filepath)
{
	initialize(filepath);

	low_img = new BlockSyncImage(base_img->get_width() / 2, base_img->get_height() / 2);
	ave();

	near_img = new BlockSyncImage(base_img->get_width(), base_img->get_height());
	near();
}

// basefilepath: 入力画像(原画像を平均画素法で縮小した画像)
// lowfilepath: 低解像度画像(入力画像を同期→平均画素法で縮小した画像)
SuperResolution::SuperResolution(char *basefilepath, char *lowfilepath)
{
	initialize(basefilepath);

	low_img = new BlockSyncImage(lowfilepath);
	near_img = new BlockSyncImage(base_img->get_width(), base_img->get_height());
	near();
}

// correctfilepath: 正解画像(原画像)
SuperResolution::SuperResolution(char *basefilepath, char *correctfilepath, char *lowfilepath) : SuperResolution(basefilepath, lowfilepath)
{
	correct_img = new BlockSyncImage(correctfilepath);
}

// デストラクタ
SuperResolution::~SuperResolution()
{
	delete base_img;
	delete low_img;
	delete near_img;
	delete output_img;
	delete correct_img;
}

// 初期化
void SuperResolution::initialize(char *basefilepath)
{
	ext_imagename(basefilepath);

	base_img = new BlockSyncImage(basefilepath);
	output_img = new BlockSyncImage(base_img->get_width() * 2, base_img->get_height() * 2);
}

// 画像の名前だけ抽出
void SuperResolution::ext_imagename(char *filepath)
{
	std::string fullpath = filepath;
	int path_length = fullpath.find_last_of("/") + 1;
	int ext_length = fullpath.find_last_of(".");
	imagename = fullpath.substr(path_length, ext_length - path_length);
}

// 平均画素法(計算式のところがたぶんまずい)
void SuperResolution::ave() {
	int sum;

	for (int i = 0; i < base_img->get_height() / 2; i++)
	{
		for (int j = 0; j < base_img->get_width() / 2; j++)
		{
			sum = base_img->get_pixel(j * 2, i * 2) + base_img->get_pixel(j * 2 + 1, i * 2) + base_img->get_pixel(j * 2 + 1, i * 2) + base_img->get_pixel(j * 2 + 1, i * 2 + 1);
			low_img->set_pixel(j, i, (int)sum / 4.0 + 0.5);
		}
	}
}

// 最近傍法(Nearest Neighbor Algorithm)
void SuperResolution::near() {
	int pix;

	// 入力画像と同じサイズの画像(入力画像を縮小→拡大)
	for (int i = 0; i < base_img->get_height() / 2; i++)
	{
		for (int j = 0; j < base_img->get_width() / 2; j++)
		{
			pix = low_img->get_pixel(j, i);
			near_img->set_pixel(j * 2, i * 2, pix);
			near_img->set_pixel(j * 2 + 1, i * 2, pix);
			near_img->set_pixel(j * 2, i * 2 + 1, pix);
			near_img->set_pixel(j * 2 + 1, i * 2 + 1, pix);
		}
	}

	// 超解像の画像
	for (int i = 0; i < base_img->get_height(); i++)
	{
		for (int j = 0; j < base_img->get_width(); j++)
		{
			pix = base_img->get_pixel(j, i);
			output_img->set_pixel(j * 2, i * 2, pix);
			output_img->set_pixel(j * 2 + 1, i * 2, pix);
			output_img->set_pixel(j * 2, i * 2 + 1, pix);
			output_img->set_pixel(j * 2 + 1, i * 2 + 1, pix);
		}
	}
}

// 引数 ks:Ksの値, kw:Kwの値, method:"psnr" または "ssim", isKs:true→Ks可変 false→Kw可変, range:可変範囲(負方向)
//
// 例
// sr(25.0, -0.033, psnr, true, 1.0) → Ks可変(25.0 〜 24.0), Kw=-0.033, psnr基準で超解像
// sr(25.0, -0.022, ssim, false, 0.10) → Ks=25.0, Kw可変(-0.022 〜 -0.032), ssim基準で超解像
//
void SuperResolution::sr(double ks, double kw, std::string method, bool isKs, double range)
{
	// psnrでもssimでもなかったら終了
	if (method != "psnr" && method != "ssim")	exit(1);

	std::function< double(BlockSyncImage&, BlockSyncImage&) > eval;
	int eval_digit;	// 評価方法(PSNR, SSIM)の小数点以下の桁数
	int k_digit;	// Ks or Kw の小数点以下の桁数

	// BlockSyncImage::psnr or BlockSyncImage::ssim の関数ポインタをつくる
	if (method == "psnr")
	{
		eval = &BlockSyncImage::calc_psnr;
		eval_digit = 4;
	}
	else if (method == "ssim")
	{
		eval = &BlockSyncImage::calc_ssim;
		eval_digit = 5;
	}

	double base_k, step, time;					// base_k:可変になるKs or Kwの基準値, step:可変値の刻み幅, time:引き込む時間
	double best_eval = 0.0, best_time, best_K;	// best_eval:最も良かった評価値, best_time/best_K:best_evalが得られた時の時間，結合強度
	std::string k_method;						// "Ks" or "Kw" (string型) 

	// 評価方法(PSNR or SSIM)のstring型の大文字をつくる
	std::string eval_method = toupper(method);
	
	std::string txt_filepath;	// テキストファイルの名前と出力場所
	std::string csv_filepath;	// CSVファイルの名前と出力場所

	// ファイル名とか使う値の初期化(KsとKwで微妙に違う)
	if (isKs)
	{
		k_method = "Ks";
		base_k = ks;
		k_digit = 2;
		step = 0.01;
		txt_filepath = "outputs/" + imagename + ", Ks=" + double_to_string(ks, 2) + "~" + double_to_string(ks - range, 2) + ", Kw=" + double_to_string(kw, 3) + "_" + eval_method +"_data.txt";
		csv_filepath = "outputs/psnr_data(" + imagename + ", Ks=" + double_to_string(ks, 2) + "~" + double_to_string(ks - range, 2) + ", Kw=" + double_to_string(kw, 3) + ").csv";
	}
	else
	{
		k_method = "Kw";
		base_k = kw;
		step = 0.001;
		k_digit = 3;
		txt_filepath = "outputs/" + imagename + ", Ks=" + double_to_string(ks, 2) + ", Kw=" + double_to_string(kw, 3) + "~" + double_to_string(kw - range, 3) + "_" + eval_method + "_data.txt";
		csv_filepath = "outputs/psnr_data(" + imagename + ", Ks=" + double_to_string(ks, 2) + ", Kw=" + double_to_string(kw, 3) + "~" + double_to_string(kw - range, 3) + ").csv";
	}
	
	// ファイルオープン
	std::ofstream writing_file;
	writing_file.open(txt_filepath, std::ios::out);
	writing_file << std::fixed << std::showpoint;
	std::ofstream csv(csv_filepath);

	// CSVファイルの一番最初の行
	csv << k_method << "," + eval_method + ", time" << std::endl;

	// 超解像
	for (double k = base_k; k >= base_k - range; k -= step)
	{
		std::string txt_buf = "";	// txtファイル用バッファ

		txt_buf += k_method + " = " + double_to_string(k, k_digit) + "\n";

		int size = base_img->get_height() * base_img->get_width();	// 
		double *b_theta = new double[size];	// 1時点前のθの配列

		// 最初は最近傍法で拡大した画像を使う
		buf_img = new BlockSyncImage(*near_img);
		buf_img->pixel_to_theta();

		// θ配列を一時保存
		for (int i = 0; i < size; i++)	b_theta[i] = buf_img->get_theta(i);

		double lowimg_best = 0.0;	// 標準画像に最も近い画像ができた時の評価値
		double lowimg_best_time;	// 標準画像に最も近い画像ができた時の引き込む時間

		delete buf_img;

		for (int t = 1; t <= 80; t++)
		{
			time = t * 0.1;

			buf_img = new BlockSyncImage(base_img->get_height(), base_img->get_width());

			// 1時点前のθをコピー
			buf_img->input_theta(b_theta);

			// 結合強度をセット
			if (isKs)
				buf_img->set_K(k, kw);
			else
				buf_img->set_K(ks, k);
			
			buf_img->resync(0.1);

			// θ配列を一時保存
			for (int i = 0; i < size; i++)	b_theta[i] = buf_img->get_theta(i);
			buf_img->theta_to_pixel();

			// 入力画像と比較してPSNR or SSIM を計算
			double tmp = eval(*base_img, *buf_img);

			// バッファに文字列足す
			txt_buf += "time = " + double_to_string(time, 2) + " : " + eval_method + " = ";
			std::string str = double_to_string(tmp, eval_digit);
			if (int n = 7 - str.length() != 0)	txt_buf += " ";	// 桁あわせ
			txt_buf += str + "\n";

			// 計算結果が良かったら更新して時間を保持
			if (tmp > lowimg_best)
			{
				lowimg_best = tmp;
				lowimg_best_time = time;
			}

			delete buf_img;
		}

		// 入力画像と比較して一番結果が良かったパラメータを使って解画像と同じサイズを作る
		buf_img = new BlockSyncImage(*output_img);
		
		if (isKs)
			buf_img->set_K(k, kw);
		else
			buf_img->set_K(ks, k);

		buf_img->pixel_to_theta();
		buf_img->resync(lowimg_best_time);
		buf_img->theta_to_pixel();

		txt_buf += k_method + " = " + double_to_string(k, k_digit) + " の時の最適解(" + std::to_string(base_img->get_width()) + "x" + std::to_string(base_img->get_height()) +")" + "\n";
		txt_buf += "time = " + double_to_string(lowimg_best_time, 1) + " : " + eval_method + " = ";
		std::string origin_best = double_to_string(lowimg_best, eval_digit);
		if (int n = 7 - origin_best.length() != 0)	txt_buf += " ";	// 桁あわせ
		txt_buf += origin_best + "\n";

		txt_buf += k_method + " = " + double_to_string(k, k_digit) + " の時の最適解(" + std::to_string(buf_img->get_width()) + "x" + std::to_string(buf_img->get_height()) + ")" + "\n";

		// 解画像と比較してPSNR or SSIM を計算
		double ans = eval(*correct_img, *buf_img);

		// バッファに文字列足す
		txt_buf += "time = " + double_to_string(lowimg_best_time, 1) + " : " + eval_method + " = ";
		std::string str = double_to_string(ans, eval_digit);
		if (int n = 7 - str.length() != 0)	txt_buf += " ";	// 桁あわせ
		txt_buf += str + "\n\n";

		// 計算結果が良かったら更新して時間を保持
		if (ans > best_eval)
		{
			best_eval = ans;
			best_time = lowimg_best_time;
			best_K = k;
		}

		// kの1ステップごとにバッファから出力
		writing_file << txt_buf;
		csv << k << "," << ans << "," << lowimg_best_time << std::endl;

		delete buf_img;
	}

	double ans = best_eval;

	double sr_time = best_time;
	// 得たパラメータ±0.2秒を調べる(そこに最適解が含まれる可能性がある)
	for (double t = (best_time - 0.2 <= 0) ? 0 : best_time - 0.2; t <= best_time + 0.2; t += 0.01)
	{
		buf_img = new BlockSyncImage(*output_img);

		if (isKs)
			buf_img->set_K(best_K, kw);
		else
			buf_img->set_K(ks, best_K);

		buf_img->pixel_to_theta();
		buf_img->resync(t);
		buf_img->theta_to_pixel();

		double tmp = eval(*correct_img, *buf_img);
		if (ans < tmp)
		{
			ans = tmp;
			sr_time = t;
		}

		delete buf_img;
	}

	// 全部通して一番良かったやつの結果を出す
	if (isKs)
		output_img->set_K(best_K, kw);
	else
		output_img->set_K(ks, best_K);

	output_img->pixel_to_theta();
	output_img->resync(sr_time);
	output_img->theta_to_pixel();

	// ファイル書き込み，出力
	std::string output_img_path;

	if (isKs)
	{
		output_img_path = "outputs/" + imagename + "_sr(Ks = " + double_to_string(best_K, 2) + ",Kw=" + double_to_string(kw, 3) + ").bmp";
		writing_file << "Ks = " << std::setprecision(2) << best_K << ", Kw = " << std::setprecision(3) << kw << ", best time = " << std::setprecision(2) << sr_time << ", " + method + " = " << std::setprecision(eval_digit) << ans << std::endl;
	}
	else
	{
		output_img_path = "outputs/" + imagename + "_sr(Ks = " + double_to_string(ks, 2) + ",Kw=" + double_to_string(best_K, 3) + ").bmp";
		writing_file << "Ks = " << std::setprecision(2) << ks << ", Kw = " << std::setprecision(3) << best_K << ", best time = " << std::setprecision(2) << sr_time << ", " + method + " = " << std::setprecision(eval_digit) << ans << std::endl;
	}

	writing_file << "比較画像との" + eval_method + "値 : " << std::setw(7) << std::right << std::setprecision(eval_digit) << ans << std::endl;

	output_img->save_bmp((char *)output_img_path.c_str());
}

void SuperResolution::sr_only(double ks, double kw, std::string method, bool isKs, double range)
{
	// psnrでもssimでもなかったら終了
	if (method != "psnr" && method != "ssim")	exit(1);

	std::function< double(BlockSyncImage&, BlockSyncImage&) > eval;
	int eval_digit;	// 評価方法(PSNR, SSIM)の小数点以下の桁数
	int k_digit;	// Ks or Kw の小数点以下の桁数

					// BlockSyncImage::psnr or BlockSyncImage::ssim の関数ポインタをつくる
	if (method == "psnr")
	{
		eval = &BlockSyncImage::calc_psnr;
		eval_digit = 4;
	}
	else if (method == "ssim")
	{
		eval = &BlockSyncImage::calc_ssim;
		eval_digit = 5;
	}

	double base_k, step, time;					// base_k:可変になるKs or Kwの基準値, step:可変値の刻み幅, time:引き込む時間
	double best_eval = 0.0, best_time, best_K;	// best_eval:最も良かった評価値, best_time/best_K:best_evalが得られた時の時間，結合強度
	std::string k_method;						// "Ks" or "Kw" (string型) 

	std::string eval_method = toupper(method);	// 評価方法(PSNR or SSIM)のstring型の大文字をつくる

	std::string txt_filepath;	// テキストファイルの名前と出力場所
	std::string csv_filepath;	// CSVファイルの名前と出力場所

	// ファイル名とか使う値の初期化(KsとKwで微妙に違う)
	if (isKs)
	{
		k_method = "Ks";
		base_k = ks;
		k_digit = 2;
		step = 0.01;
		txt_filepath = "outputs/" + imagename + ", Ks=" + double_to_string(ks, 2) + "~" + double_to_string(ks - range, 2) + ", Kw=" + double_to_string(kw, 3) + "_" + eval_method + "_data.txt";
		csv_filepath = "outputs/psnr_data(" + imagename + ", Ks=" + double_to_string(ks, 2) + "~" + double_to_string(ks - range, 2) + ", Kw=" + double_to_string(kw, 3) + ").csv";
	}
	else
	{
		k_method = "Kw";
		base_k = kw;
		step = 0.001;
		k_digit = 3;
		txt_filepath = "outputs/" + imagename + ", Ks=" + double_to_string(ks, 2) + ", Kw=" + double_to_string(kw, 3) + "~" + double_to_string(kw - range, 3) + "_" + eval_method + "_data.txt";
		csv_filepath = "outputs/psnr_data(" + imagename + ", Ks=" + double_to_string(ks, 2) + ", Kw=" + double_to_string(kw, 3) + "~" + double_to_string(kw - range, 3) + ").csv";
	}

	// ファイルオープン
	std::ofstream writing_file;
	writing_file.open(txt_filepath, std::ios::out);
	writing_file << std::fixed << std::showpoint;
	std::ofstream csv(csv_filepath);

	// CSVファイルの一番最初の行
	csv << k_method << "," + eval_method + "(標準画像と入力画像間), time" << std::endl;

	// 超解像
	for (double k = base_k; k >= base_k - range; k -= step)
	{
		std::string txt_buf = "";	// txtファイル用バッファ

		txt_buf += k_method + " = " + double_to_string(k, k_digit) + "\n";

		int size = base_img->get_height() * base_img->get_width();
		double *b_theta = new double[size];			// 1時点前のθの配列

		buf_img = new BlockSyncImage(*near_img);	// 最初は最近傍法で拡大した画像を使う
		buf_img->pixel_to_theta();

		// θ配列を一時保存
		for (int i = 0; i < size; i++)	b_theta[i] = buf_img->get_theta(i);

		double lowimg_best = 0.0;	// 標準画像に最も近い画像ができた時の評価値
		double lowimg_best_time;	// 標準画像に最も近い画像ができた時の引き込む時間

		delete buf_img;

		for (int t = 1; t <= 80; t++)
		{
			time = t * 0.1;

			buf_img = new BlockSyncImage(base_img->get_height(), base_img->get_width());

			// 1時点前のθをコピー
			buf_img->input_theta(b_theta);

			// 結合強度をセット
			if (isKs)
				buf_img->set_K(k, kw);
			else
				buf_img->set_K(ks, k);

			buf_img->resync(0.1);

			// θ配列を一時保存
			for (int i = 0; i < size; i++)	b_theta[i] = buf_img->get_theta(i);
			buf_img->theta_to_pixel();

			// 入力画像と比較してPSNR or SSIM を計算
			double tmp = eval(*base_img, *buf_img);

			// バッファに文字列足す
			txt_buf += "time = " + double_to_string(time, 1) + " : " + eval_method + " = ";
			std::string str = double_to_string(tmp, eval_digit);
			if (int n = 7 - str.length() != 0)	txt_buf += " ";	// 桁あわせ
			txt_buf += str + "\n";

			// 計算結果が良かったら更新して時間を保持
			if (tmp > lowimg_best)
			{
				lowimg_best = tmp;
				lowimg_best_time = time;
			}

			delete buf_img;
		}

		if (lowimg_best > best_eval)
		{
			best_eval = lowimg_best;
			best_time = lowimg_best_time;
			best_K = k;
		}

		// 入力画像と比較して一番結果が良かったパラメータを使って解画像と同じサイズを作る
		buf_img = new BlockSyncImage(*output_img);

		if (isKs)
			buf_img->set_K(k, kw);
		else
			buf_img->set_K(ks, k);

		buf_img->pixel_to_theta();
		buf_img->resync(lowimg_best_time);
		buf_img->theta_to_pixel();

		txt_buf += k_method + " = " + double_to_string(k, k_digit) + " の時の最適解" + "\n\n";

		// kの1ステップごとにバッファから出力
		writing_file << txt_buf;
		csv << k << "," << lowimg_best << "," << lowimg_best_time << std::endl;

		delete buf_img;
	}

	// 全部通して一番良かったやつの結果を出す
	if (isKs)
		output_img->set_K(best_K, kw);
	else
		output_img->set_K(ks, best_K);

	output_img->pixel_to_theta();
	output_img->resync(best_time);
	output_img->theta_to_pixel();

	// ファイル書き込み，出力
	std::string output_img_path;

	if (isKs)
	{
		output_img_path = "outputs/" + imagename + "_sr(Ks = " + double_to_string(best_K, 2) + ",Kw=" + double_to_string(kw, 3) + ").bmp";
		writing_file << "Ks = " << std::setprecision(2) << best_K << ", Kw = " << std::setprecision(3) << kw << ", best time = " << std::setprecision(1) << best_time << ", " + method + " = " << std::setprecision(eval_digit) << best_eval << std::endl;
	}
	else
	{
		output_img_path = "outputs/" + imagename + "_sr(Ks = " + double_to_string(ks, 2) + ",Kw=" + double_to_string(best_K, 3) + ").bmp";
		writing_file << "Ks = " << std::setprecision(2) << ks << ", Kw = " << std::setprecision(3) << best_K << ", best time = " << std::setprecision(1) << best_time << ", " + method + " = " << std::setprecision(eval_digit) << best_eval << std::endl;
	}

	output_img->save_bmp((char *)output_img_path.c_str());
}

void SuperResolution::sr(double Ks, double Kw, std::string method)
{
	// psnrでもssimでもなかったら終了
	if (method != "psnr" && method != "ssim")	exit(1);

	// BlockSyncImage::calc_psnr or BlockSyncImage::calc_ssim の関数ポインタをつくる
	std::function< double(BlockSyncImage&, BlockSyncImage&) > eval;
	if (method == "psnr")		eval = &BlockSyncImage::calc_psnr;
	else if (method == "ssim")	eval = &BlockSyncImage::calc_ssim;

	double time;
	double lowimg_best = 0.0;
	double best = 0.0, best_time;

	std::string eval_method = toupper(method);

	std::string txt_filepath = "outputs/" + imagename + "_" + eval_method + "_data.txt";
	std::ofstream writing_file;
	writing_file.open(txt_filepath, std::ios::out);

	for (int t = 1; t < 80; t++)
	{
		time = 0.1 * t;

		buf_img = new BlockSyncImage(*near_img);
		buf_img->set_K(Ks, Kw);
		buf_img->resync(time);

		double tmp = eval(*base_img, *buf_img);
		std::cout << eval_method + " = " << tmp << std::endl;
		writing_file << eval_method + " = " << tmp << std::endl;

		if (tmp > lowimg_best)
		{
			lowimg_best = tmp;
			best_time = time;
		}

		delete buf_img;
	}
	std::cout << std::endl;
	writing_file << std::endl;

	buf_img = new BlockSyncImage(*output_img);
	buf_img->set_K(Ks, Kw);
	buf_img->resync(best_time);
	std::string filepath = "outputs/" + imagename + "_sr(Ks = " + double_to_string(Ks, 2) + ", Kw = " + double_to_string(Kw, 3) + ").bmp";
	buf_img->save_bmp((char *)filepath.c_str());

	std::cout << "Ks = " << Ks << "Kw = " << Kw << "time = " << best_time << std::endl;
	writing_file << "Ks = " << Ks << ", Kw = " << Kw << ", time = " << best_time << std::endl;

	double psnr = correct_img->calc_psnr(*output_img);
	std::cout << "PSNR = " << psnr << std::endl;
	writing_file << "PSNR = " << psnr << std::endl;

	double ssim = correct_img->calc_ssim(*output_img);
	std::cout << "SSIM = " << ssim << std::endl;
	writing_file << "SSIM = " << ssim << std::endl;
}

// 結合強度，時間 全指定版 パラメータが分かっていて画像だけ欲しい時に
void SuperResolution::sr(double Ks, double Kw, double time)
{
	std::string txt_filepath = "outputs/" + imagename + "_data.txt";
	std::ofstream writing_file;
	writing_file.open(txt_filepath, std::ios::out);

	output_img->set_K(Ks, Kw);
	output_img->resync(time);
	std::string filepath = "outputs/" + imagename + "_sr(Ks = " + double_to_string(Ks, 2) + ", Kw = " + double_to_string(Kw, 3) + ", time = " + double_to_string(time, 3) + ").bmp";
	output_img->save_bmp((char *)filepath.c_str());

	std::cout << "Ks = " << Ks << "Kw = " << Kw << "time = " << time << std::endl;
	writing_file << "Ks = " << Ks << ", Kw = " << Kw << ", time = " << time << std::endl;

	//double psnr = correct_img->calc_psnr(*output_img);
	//std::cout << "PSNR = " << psnr << std::endl;
	//writing_file << "PSNR = " << psnr << std::endl;

	//double ssim = correct_img->calc_ssim(*output_img);
	//std::cout << "SSIM = " << ssim << std::endl;
	//writing_file << "SSIM = " << ssim << std::endl;
}

string SuperResolution::double_to_string(double d, int digits)
{
	std::ostringstream oss;
	oss << setprecision(digits) << setiosflags(ios::fixed) << d;
	return oss.str();
}

string SuperResolution::toupper(string str)
{
	std::string upper;
	upper.resize(str.size());
	std::transform(str.begin(), str.end(), upper.begin(), ::toupper);
	return upper;
}