#include "../header/BlockSyncImage.h"
#include <omp.h>

void BlockSyncImage::init(){
	W = width;
	H = height;
	N = W*H;
	theta = new double[N];
	buf_theta = new double[N];
	k1 = new double[N];
	k2 = new double[N];
	k3 = new double[N];
	k4 = new double[N];
	buf_k = new double[N];
	pixel_to_theta();
	for (int i = 0; i < 256; i++)
	{
		double quan_buf = (double)(256 - 1) / 2;
		pixel_radian[i] = asin((double)(i - quan_buf) / quan_buf);
	}
}

void BlockSyncImage::set_K(double Ks, double Kw){
	for (int i = 0; i < 4; i++){
		BlockSyncImage::Ks[i] = Ks;
		BlockSyncImage::Kw[i] = Kw;
	}
}

void BlockSyncImage::set_K(double Ks[4], double Kw[4]){
	for (int i = 0; i < 4; i++){
		BlockSyncImage::Ks[i] = Ks[i];
		BlockSyncImage::Kw[i] = Kw[i];
	}
}

void BlockSyncImage::pixel_to_theta(void)
{
	double quan_buf = (double)(256 - 1) / 2;
	for (int i = 0; i<N; i++)
		theta[i] = asin((double)(pixels[i].pix - quan_buf) / quan_buf);
}

void BlockSyncImage::theta_to_pixel(void)
{
	for (int i = 0; i < N; i++) 
	{

		if (theta[i] > M_PI / 2.0 || theta[i] < -M_PI / 2.0)
		{
			theta[i] = asin(sin(theta[i]));
		}
		pixels[i].pix = (int)(127.5 + 127.5*sin(theta[i]) + 0.5);// +0.5は四捨五入の為（キャストの仕様は切り捨て）
	}
}

// Runge-Kuttaの方法に用いるためのkn(n=1〜4)を求める
void BlockSyncImage::calc_kn(int n){
	
	switch (n){
	case 1:
		for (int i = 0; i < N; i++)
			buf_theta[i] = theta[i];
		break;
	case 2:
		for (int i = 0; i < N; i++)
			buf_theta[i] = theta[i] + dt*k1[i] / 2.0;
		break;
	case 3:
		for (int i = 0; i < N; i++)
			buf_theta[i] = theta[i] + dt*k2[i] / 2.0;
		break;
	case 4:
		for (int i = 0; i < N; i++)
			buf_theta[i] = theta[i] + dt*k3[i];
		break;
	}

		#pragma omp parallel for
		for (int i = 0; i < H; i++)
		{
			#pragma omp parallel for
				for (int j = 0; j < W; j++)
				{
					double current_theta = buf_theta[XY(j, i)];

					// 画像の四隅の画素
					// 左上端
					if (i == 0 && j == 0) {
						buf_k[XY(j, i)] = Ks[0] * sin(buf_theta[XY(j + 1, i)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Ks[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 4.0;
					}
					// 右上端
					else if (i == 0 && j == W - 1) {
						buf_k[XY(j, i)] = Ks[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Ks[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 4.0;
					}
					// 左下端
					else if (i == H - 1 && j == 0) {
						buf_k[XY(j, i)] = Ks[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Ks[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta) + Ks[0] * sin(buf_theta[XY(j + 1, i)] - current_theta);
						buf_k[XY(j, i)] /= 4.0;
					}
					// 右下端
					else if (i == H - 1 && j == W - 1) {
						buf_k[XY(j, i)] = Ks[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Ks[0] * sin(buf_theta[XY(j - 1, i)] - current_theta);
						buf_k[XY(j, i)] /= 4.0;
					}

					// 画像の上端の画素
					else if (i == 0 && j % 2 == 0) {
						buf_k[XY(j, i)] = Kw[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Ks[0] * sin(buf_theta[XY(j + 1, i)] - current_theta)
							+ Kw[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Ks[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 6.0;
					}
					else if (i == 0 && j % 2 == 1) {
						buf_k[XY(j, i)] = Ks[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Kw[0] * sin(buf_theta[XY(j + 1, i)] - current_theta)
							+ Ks[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Kw[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 6.0;
					}

					// 画像の下端の画素
					else if (i == H - 1 && j % 2 == 0) {
						buf_k[XY(j, i)] = Kw[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Ks[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta)
							+ Kw[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Ks[0] * sin(buf_theta[XY(j + 1, i)] - current_theta);
						buf_k[XY(j, i)] /= 6.0;
					}
					else if (i == H - 1 && j % 2 == 1) {
						buf_k[XY(j, i)] = Ks[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Kw[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta)
							+ Ks[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Kw[0] * sin(buf_theta[XY(j + 1, i)] - current_theta);
						buf_k[XY(j, i)] /= 6.0;
					}

					// 画像の左端の画素
					else if (i % 2 == 0 && j == 0) {
						buf_k[XY(j, i)] = Kw[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Kw[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta)
							+ Ks[0] * sin(buf_theta[XY(j + 1, i)] - current_theta)
							+ Ks[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Ks[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 6.0;
					}
					else if (i % 2 == 1 && j == 0) {
						buf_k[XY(j, i)] = Ks[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Ks[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta)
							+ Ks[0] * sin(buf_theta[XY(j + 1, i)] - current_theta)
							+ Kw[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Kw[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 6.0;
					}

					// 画像の右端の画素
					else if (i % 2 == 0 && j == W - 1) {
						buf_k[XY(j, i)] = Kw[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Kw[2] * sin(buf_theta[XY(j, i - 1)] - current_theta)
							+ Ks[0] * sin(buf_theta[XY(j - 1, i)] - current_theta)
							+ Ks[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 6.0;
					}
					else if (i % 2 == 1 && j == W - 1) {
						buf_k[XY(j, i)] = Ks[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i - 1)] - current_theta)
							+ Ks[0] * sin(buf_theta[XY(j - 1, i)] - current_theta)
							+ Kw[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Kw[2] * sin(buf_theta[XY(j, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 6.0;
					}

					//if (i == 0 || i == H - 1 || j == 0 || j == W - 1)	buf_k[XY(j, i)] = current_theta;

					// 端以外の画素の場合
					// 左上
					else if (i % 2 == 0 && j % 2 == 0) {
						buf_k[XY(j, i)] = Kw[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Kw[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Kw[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta)
							+ Kw[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Ks[0] * sin(buf_theta[XY(j + 1, i)] - current_theta)
							+ Kw[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Ks[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 9.0;
					}
					// 右上
					else if (i % 2 == 0 && j % 2 == 1) {
						buf_k[XY(j, i)] = Kw[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Kw[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Kw[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta)
							+ Ks[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Kw[0] * sin(buf_theta[XY(j + 1, i)] - current_theta)
							+ Ks[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Kw[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 9.0;
					}
					// 左下
					else if (i % 2 == 1 && j % 2 == 0) {
						buf_k[XY(j, i)] = Kw[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Ks[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta)
							+ Kw[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Ks[0] * sin(buf_theta[XY(j + 1, i)] - current_theta)
							+ Kw[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Kw[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Kw[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 9.0;
					}
					// 右下
					else if (i % 2 == 1 && j % 2 == 1) {
						buf_k[XY(j, i)] = Ks[3] * sin(buf_theta[XY(j - 1, i - 1)] - current_theta) + Ks[2] * sin(buf_theta[XY(j, i - 1)] - current_theta) + Kw[1] * sin(buf_theta[XY(j + 1, i - 1)] - current_theta)
							+ Ks[0] * sin(buf_theta[XY(j - 1, i)] - current_theta) + Kw[0] * sin(buf_theta[XY(j + 1, i)] - current_theta)
							+ Kw[1] * sin(buf_theta[XY(j - 1, i + 1)] - current_theta) + Kw[2] * sin(buf_theta[XY(j, i + 1)] - current_theta) + Kw[3] * sin(buf_theta[XY(j + 1, i + 1)] - current_theta);
						buf_k[XY(j, i)] /= 9.0;
					}
				}
			}

		//#pragma omp parallel for
		/*for (int i = 0; i < H; i++)
		{
			#pragma omp parallel for
			for (int j = 0; j < W; j++)
			{
				int index = XY(j, i);
				double current_theta = buf_theta[index];
				buf_k[index] = 0.0;
				
				for (int m = -2; m <= 2; m++)
				{
					for (int n = -2; n <= 2; n++)
					{
						double K;

						if (2 == abs(m) || 2 == abs(n))
							K = Kw[0];
						else if (1 == abs(m) || 1 == abs(n))
							K = Ks[0];
						else
							continue;

						buf_k[index] += K * sin(buf_theta[XY(HLImage::reflect(j + n, 0, W), HLImage::reflect(i + m, 0, H))] - current_theta);
					}
				}

				buf_k[index] /= 25.0;
			}
		}*/

	switch (n){
	case 1:
		for (int i = 0; i < N; i++)
			k1[i] = buf_k[i];
		break;
	case 2:
		for (int i = 0; i < N; i++)
			k2[i] = buf_k[i];
		break;
	case 3:
		for (int i = 0; i < N; i++)
			k3[i] = buf_k[i];
		break;
	case 4:		
		for (int i = 0; i < N; i++)
			k4[i] = buf_k[i];
		break;
	default:
		cout << "errorkamo1";
		break;
	}
}

void BlockSyncImage::rungekutta(bool timeforward){
	// k1~k4を求める
	calc_kn(1);
	calc_kn(2);
	calc_kn(3);
	calc_kn(4);

	// 時間を進める(戻す)
	for (int i = 0; i < N; i++){
		if (timeforward)
			theta[i] += dt*(k1[i] + 2.0*k2[i] + 2.0*k3[i] + k4[i]) / 6.0;
		else
			theta[i] -= dt*(k1[i] + 2.0*k2[i] + 2.0*k3[i] + k4[i]) / 6.0;
	}
}

// 引き込み
void BlockSyncImage::sync(double time){
	pixel_to_theta();
	for (double t = 0.0; t < time; t += dt){
		rungekutta(true);
	}
	theta_to_pixel();
}

// 逆引き込み
void BlockSyncImage::resync(double time){
	for (double t = 0.0; t < time; t += dt){
		rungekutta(false);
	}
}

void BlockSyncImage::input_theta(double *source)
{
	for (int i = 0; i < N; i++)	theta[i] = source[i];
}

BlockSyncImage::~BlockSyncImage()
{
	delete[] theta;
	delete[] buf_theta;
	delete[] k1, k2, k3, k4, buf_k;
}
